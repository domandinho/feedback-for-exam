from django.conf.urls import include, url
from django.contrib import admin
from ownapp.views import pagination, searchdata

# Examples:
# url(r'^$', 'myproject.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    url(r'^$', pagination),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/', searchdata),
    url(r'^record/page/(?P<page_num>\d+)/$', pagination)

]
