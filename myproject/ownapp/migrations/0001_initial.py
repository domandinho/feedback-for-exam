# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imie', models.CharField(max_length=1000)),
                ('nazwisko', models.CharField(max_length=1000)),
                ('numerIndeksu', models.IntegerField(validators=django.core.validators.RegexValidator(regex=b'\\d{6, 6}'))),
            ],
        ),
    ]
