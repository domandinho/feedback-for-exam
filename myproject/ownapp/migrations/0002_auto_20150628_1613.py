# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('ownapp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='id',
        ),
        migrations.AlterField(
            model_name='student',
            name='numerIndeksu',
            field=models.IntegerField(serialize=False, primary_key=True, validators=django.core.validators.RegexValidator(regex=b'\\d\\d\\d\\d\\d\\d')),
        ),
    ]
