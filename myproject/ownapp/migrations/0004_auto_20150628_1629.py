# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ownapp', '0003_auto_20150628_1626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='imie',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='nazwisko',
            field=models.CharField(max_length=1000, null=True),
        ),
    ]
