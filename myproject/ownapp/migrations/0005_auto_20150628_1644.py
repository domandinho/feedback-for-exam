# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('ownapp', '0004_auto_20150628_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='numerIndeksu',
            field=models.IntegerField(serialize=False, primary_key=True, validators=[django.core.validators.RegexValidator(b'\\d\\d\\d\\d\\d\\d')]),
        ),
    ]
