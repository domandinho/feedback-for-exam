# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('ownapp', '0005_auto_20150628_1644'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zadanie',
            fields=[
                ('numer', models.IntegerField(serialize=False, primary_key=True)),
                ('dataOddania', models.DateField()),
            ],
        ),
        migrations.AlterField(
            model_name='student',
            name='numerIndeksu',
            field=models.IntegerField(serialize=False, primary_key=True, validators=[django.core.validators.RegexValidator(b'^\\d{6,6}$')]),
        ),
    ]
