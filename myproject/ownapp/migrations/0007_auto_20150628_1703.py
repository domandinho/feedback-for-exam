# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ownapp', '0006_auto_20150628_1653'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rozwiazanie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dataoddania', models.DateField()),
                ('student', models.ForeignKey(to='ownapp.Student')),
                ('zadanie', models.ForeignKey(to='ownapp.Zadanie')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='rozwiazanie',
            unique_together=set([('student', 'zadanie')]),
        ),
    ]
