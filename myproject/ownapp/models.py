from django.db import models
from django.core.validators import *;


def validateIndexNumber(value):
    return False


class Student(models.Model):
    imie = models.CharField(max_length=1000, null=True)
    nazwisko = models.CharField(max_length=1000, null=True)
    numerIndeksu = models.IntegerField(validators=[RegexValidator('^\d{6,6}$')], primary_key=True)

    def __str__(self):
        return '[' + str(self.numerIndeksu) + ']' + self.imie + ' ' + self.nazwisko


class Zadanie(models.Model):
    numer = models.IntegerField(primary_key=True)
    dataOddania = models.DateField()

    def __str__(self):
        return '[' + str(self.numer) + '] ' + str(self.dataOddania)


class Rozwiazanie(models.Model):
    class Meta:
        unique_together = ['student', 'zadanie']

    student = models.ForeignKey(Student)
    zadanie = models.ForeignKey(Zadanie)
    dataoddania = models.DateField(null=False)

    def __str__(self):
        return '[' + str(self.student.numerIndeksu) + '] ' + str(self.zadanie.numer) + ' ' + str(self.dataoddania)
