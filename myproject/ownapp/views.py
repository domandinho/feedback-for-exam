from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response
from models import Rozwiazanie, Zadanie, Student
from django.http import HttpResponse
import json


class ImportantData:
    student = ''
    tasks = ''
    delay = ''

    def __init__(self, student, tasks, delay):
        self.student = student
        self.tasks = tasks
        self.delay = delay


def createResultTable():
    result_list = []
    student_list = Student.objects.all().order_by('numerIndeksu')
    for student in student_list:
        numberOfSolutions = 0
        delay = 0
        for solution in Rozwiazanie.objects.all():
            if (solution.student == student):
                numberOfSolutions = numberOfSolutions + 1
                delay = delay + (solution.dataoddania - solution.zadanie.dataOddania).days
        studentString = str(student.numerIndeksu) + '(' + str(student.imie) + ', ' + str(student.nazwisko) + ')'
        data = ImportantData(student=studentString, tasks=str(numberOfSolutions), delay=str(delay))
        result_list.append(data)
    return result_list


def pagination(request, page_num=1):
    size = 2
    resultList = createResultTable()
    paginator = Paginator(resultList, size)
    try:
        datatable = paginator.page(page_num)
    except PageNotAnInteger:
        datatable = paginator.page(1)
    except EmptyPage:
        datatable = paginator.page(paginator.num_pages)
    context = {
        "datatable": datatable,
        "page_num": page_num,
        "page_max": paginator.num_pages
    }
    context['loop_times'] = range(1, 10)
    return render_to_response('ownapp/pagination.html', context)


def generateListOfStudentsForSearchQuery(querycontent):
    list_of_students = []
    for student in Student.objects.all():
        studentFirstNameLower = student.imie.lower()
        studentNameLower = student.nazwisko.lower()
        studentIndexNumberText = str(student.numerIndeksu)
        if (querycontent in studentNameLower) or (querycontent in studentFirstNameLower) or (
                    querycontent in studentIndexNumberText):
            list_of_students.append(student.imie)
            list_of_students.append(student.nazwisko)
            list_of_students.append(student.numerIndeksu)
    return list_of_students;


# not used it is only example of AJAX
def searchdata(request):
    querycontent = request.POST.get('querycontent')
    lowerquerycontent = querycontent.lower()
    searchtable = generateListOfStudentsForSearchQuery(lowerquerycontent)
    context = {
        "searchtable": searchtable
    }
    return HttpResponse(
        json.dumps(context),
        content_type="application/json"
    )

